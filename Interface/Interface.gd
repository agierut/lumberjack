extends Control

signal ax_changed()
var ROOT = null

func _ready():
	ROOT = get_tree().get_root().get_node("World")
	
# TODO add game over
func _decrease_health(body):
	# decrease the number of lifes if enemy was touched by player
	if "Enemy" in body.get_name():
		var hearts_number = get_node("Lifebar/Hearts")
		hearts_number.value = hearts_number.value - 1
	
		# if number of lifes is equal to 0, then its game over
		if hearts_number.value == 0:
			ROOT.game_over()

func _increase_axes_counter(body):
	# increase the axes counter
	if body.name == "Player":
		var axes_counter = get_node("AxCounter/Label")
		axes_counter.text = str(int(axes_counter.text) + 1)

