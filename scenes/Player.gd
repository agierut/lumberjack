extends KinematicBody2D

const WALK_SPEED = 300
const JUMP_POWER = 600
const GRAVITY = 20
const FLOOR = Vector2(0, -1)

var velocity = Vector2()
var health = 100

func _ready():
	set_physics_process(true) # tells node that should be running the fixed process function

# TODO jump animation
func _physics_process(delta):
	# which button was pressed (can be combined)
	var pressed_left = Input.is_action_pressed("ui_left")
	var pressed_right = Input.is_action_pressed("ui_right")
	var pressed_up  = Input.is_action_pressed("ui_up")
	var is_being_hit = get_node("PlayerAnimation").is_playing()
	
	# Xaxis - increase speed depending on direction (right, left)
	if pressed_right:
		velocity.x = WALK_SPEED
		$PlayerSprite.flip_h = false;
		# play the sprite on a condition that player didn't hit the enemy
		# cuz then the hit animation is playing
		if !is_being_hit:
			$PlayerSprite.play("right")
	elif pressed_left:
		velocity.x = -WALK_SPEED
		$PlayerSprite.flip_h = true;
		if !is_being_hit:
			$PlayerSprite.play("right")
	else:
		velocity.x = 0
		if !is_being_hit:
			$PlayerSprite.play("idle")
		
	# Yaxis - perform jump ONLY if player is on the floor
	if pressed_up and is_on_floor():
		velocity.y = -JUMP_POWER
	velocity.y += GRAVITY
	
	# animate jump
	if !is_on_floor():
		if pressed_left:
			$PlayerSprite.flip_h = true;
			#$Sprite.play("jump_right")
		elif pressed_right:
			$PlayerSprite.flip_h = false;
			#$Sprite.play("jump_right")
		else:
			if !is_being_hit:
				$PlayerSprite.play("idle")
			pass

	velocity = move_and_slide(velocity, FLOOR)

func _on_PlayerArea2D_body_entered(body):
	# if player touched the enemy, animate the hit
	if "Enemy" in body.name:
		get_node("PlayerAnimation").play("hit")
		get_node("PlayerSound").play()
