extends Area2D

func _ready():
	var interf = get_parent().get_parent().get_node("Interface")
	self.connect("body_entered", interf, "_increase_axes_counter")