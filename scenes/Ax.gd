extends Node2D

func _ready():
	if get_parent() != null:
		get_parent().axes_total += 1
	
func _collect_ax(body):
	# axe is collected only if player entered the area
	if "Player" in body.get_name():
		var playSound = get_node("AxeSound").play()
		
		get_node("AxeAnimation").play("disappear")
		
		if get_owner() != null:
			get_owner().axes_collected += 1	
	
func delete_node():
	queue_free()