extends Node2D

const MAX_X = 760 # 608 height
const MIN_Y = 390
const MIN_PLATFORM_WIDTH = 2
const MAX_PLATFORM_WIDTH = 10
const BOTTOM_BORDER = 360
const PLAYER_JUMP = -140 # can jump 146px
var CAMERA_SPEED = 1.7

# timers data
const PLATFORM_TIMER = 1.3
const ENEMY_TIMER = 3.0
const AX_TIMER = 7.0

var _axe_timer = null
var _enemy_timer = null
var _platform_timer = null
var timers_started = false

# var for the newest, highest platform
var highest_platform = null

# ax collection
var axes_total = 0
var axes_collected = 0

# Tscn models
var axes_loaded = load("res://scenes/Axe.tscn")
var enemy_loaded = load("res://scenes/Enemy.tscn")
var platform_loaded = []

func _ready():
	# to get random number every time the game launches
	randomize()
	load_resources()
	_physics_process(true)
	
func load_resources():
	# load a couple of platforms with width 2,4,6,8 and 10 into array
	for i in range(2,12,2):
		var res = load("res://scenes/platforms/" + str(i) +  "Platform.tscn")	
		platform_loaded.append(res)
		
func _physics_process(delta):
	var player_y = get_tree().get_root().get_node("World").get_node("Player").position.y
	# start the timers if player jumped above the middle height
	if player_y < -15 and !timers_started:
		# start the timers
		_init_timers(_platform_timer, PLATFORM_TIMER, "_randomise_platforms")
		_init_timers(_axe_timer, AX_TIMER, "_randomise_ax")
		_init_timers(_enemy_timer, ENEMY_TIMER, "_randomise_enemy")
		timers_started = true
	
func _init_timers(node, timer, method):
	# initialize timers which will generate nodes - enemy, axe and platform
	node = Timer.new()
	add_child(node)
	
	node.connect("timeout", self, method)
	node.set_wait_time(timer)
	node.set_one_shot(false) # make sure it loops
	node.start()
	
# TODO err platforms generate outside the viewport
func _randomise_platforms():
	# generate a new platform with random pos and width above the viewport
	var newest_platform = get_children_of_type(self, "Platform")[-1]
	var platform_width = randi() % MAX_PLATFORM_WIDTH
	while platform_width % 2 != 0:
		platform_width = randi() % MAX_PLATFORM_WIDTH
	if platform_width == 0:
		platform_width = 10

	var new_x = randi() % (370 - 60 * platform_width)	
	var new_y = -335

	var pos_arr = (platform_width - 2)/2
	highest_platform = generate_node(platform_loaded[pos_arr], new_x, new_y)

func get_children_of_type(basecl, desicl):
	# returns a list of children with desired name
	var n = []
	for c in basecl.get_children():
		if desicl in c.name:
			n.append(c)
	return n	
	
func _randomise_enemy():
	var enemy_ref = weakref(highest_platform).get_ref()
	
	# create enemy only on platforms which width is bigger than 2 rectangles
	# and only if there's still reference to the highest_platform
	if enemy_ref and !"2" in highest_platform.name:
		# create enemy node on the newest platform
		var enemy_x = highest_platform.position.x- 110
		var enemy_y = highest_platform.position.y - 58
		
		generate_node(enemy_loaded, enemy_x, enemy_y)
	
func _randomise_ax():
	# if element ax was not deleted/freed, generate new ax
	var ax_ref = weakref(highest_platform).get_ref()
	
	if ax_ref:
		# create an ax node every AX_TIMER seconds on a random position on highest platform
		var platform_width = highest_platform.name[0]
		if "@" in platform_width:
			platform_width = int(highest_platform.name[1])
		elif "1" in platform_width:
			platform_width = 10
		else:
			platform_width = int(highest_platform.name[0])

		var ax_x = randi() % int(highest_platform.position.x + 30*platform_width) + highest_platform.position.x - 30*platform_width
		var ax_y = highest_platform.position.y - 60
		
		generate_node(axes_loaded, ax_x, ax_y)
	
func generate_node(nodeinst, x, y):
	# generate and return a new instance of node in given position
	var new_node = nodeinst.instance()
	new_node.position = Vector2(x, y)
	self.add_child(new_node)	
	return new_node
	
func game_over():
	print ("### game over")
	get_tree().paused = true
	add_child(load("res://scenes/DeathMenu/DeathMenu.tscn"))
	