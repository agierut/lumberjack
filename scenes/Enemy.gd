extends KinematicBody2D

const WALK_SPEED = 150
const GRAVITY = 20
const FLOOR = Vector2(0, -1) # which surface to detect as floor

var velocity = Vector2()
var enemy_dir = 1 # right

func _ready():
	pass

func _physics_process(delta):
	# animate and change direction of sprite if needed
	$EnemyAnimation.play("walk")
	if enemy_dir == 1:
		$EnemyAnimation.flip_h = false;
	else:
		$EnemyAnimation.flip_h = true;
	
	# move enemy
	velocity.x = WALK_SPEED * enemy_dir
	velocity.y += GRAVITY
	velocity = move_and_slide(velocity, FLOOR)

	# if enemy hit the wall, change direction
	if is_on_wall():
		enemy_dir = -enemy_dir
		$EnemyRayCast2D.position.x *= -1
		
	# if enemy is on the edge of platform - the raycast is not colliding -  we hit the end
	if $EnemyRayCast2D.is_colliding() == false:
		enemy_dir = -enemy_dir
		$EnemyRayCast2D.position.x *= -1
		