extends Camera2D

const IS_SCROLLING = false

var ROOT = null
var camera_speed = null

func _ready():
	ROOT = get_tree().get_root().get_node("World")
	camera_speed = ROOT.CAMERA_SPEED
	set_process(true)

func _process(delta):
	var player_y = get_tree().get_root().get_node("World").get_node("Player").position.y
	
	# start scrolling background if player jumped above the middle height
	if player_y < -15:
		IS_SCROLLING = true
	scroll_background()
		
func scroll_background():
	if IS_SCROLLING:
		var root_children = ROOT.get_children()
		var bottom_border = ROOT.BOTTOM_BORDER
		
		for node in root_children:
			# don't updatethe position of interface, background and timers since they don't contain position attributes
			if (node.name != "Interface" and node.name != "ParallaxBackground" and node.name != "Timer" 
			and !"@@" in node.name and node.name != "WallsCollision"):
				# update position of every element on screen
				node.position = Vector2(node.position.x, node.position.y + camera_speed)
				
				# if player crossed the bottom border, the game ends
				if node.position.y > bottom_border and node.name == "Player":
					ROOT.game_over()
				
				# if object is below bottom border, it is deleted
				if node.position.y > bottom_border:
					node.queue_free()
